﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using PatientCards.Domain;
using PatientCards.Settings;

namespace PatientCards.Repository
{
    public class ApplicationDbContext : DbContext
    {
        private readonly SettingsProvider _settingsProvider;
        public DbSet<Patient> Patients { get; set; } = null!;
        public DbSet<Disease> Diseases { get; set; } = null!;
        public DbSet<Visit> Visits { get; set; } = null!;

        public ApplicationDbContext(SettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
            //Database.EnsureDeleted();
            Database.EnsureCreated();

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=Patients_db;Trusted_Connection=True;");
            //optionsBuilder.UseSqlServer(InitialSettings.LoadConnectionStringFromConfig("MSSQLConnection"));
            //optionsBuilder.UseSqlite(InitialSettings.LoadConnectionStringFromConfig("SQLiteConnection"));
            optionsBuilder.UseSqlite(_settingsProvider.GetConnectionString());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
         
        }
    }
}
