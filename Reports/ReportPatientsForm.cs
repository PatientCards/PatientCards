﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientCards.Repository;
//using PatientCards.Reports;

namespace PatientCards
{
    public partial class ReportPatientsForm : Form
    {
        private readonly RepositoryDB _repositoryDB;
        public ReportPatientsForm(RepositoryDB repositoryDB)
        {
            InitializeComponent();
            _repositoryDB = repositoryDB;
        }

        private void ReportPatientsForm_Shown(object sender, EventArgs e)
        {
            dataGridViewAllPatientsReport.DataSource = _repositoryDB.GetAllPatients();
        }
    }
}
