using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PatientCards.Forms;
using PatientCards.Repository;
using System.Configuration;

namespace PatientCards
{
    //internal static class Program
    //{
    //    /// <summary>
    //    ///  The main entry point for the application.
    //    /// </summary>
    //    [STAThread]
    //    static void Main()
    //    {
    //        // To customize application configuration such as set high DPI settings or default font,
    //        // see https://aka.ms/applicationconfiguration.
    //        ApplicationConfiguration.Initialize();
    //        Application.Run(new MainForm());


    //    }
    //}
    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            ApplicationConfiguration.Initialize();

            var hostBuilder = Host.CreateDefaultBuilder();

            hostBuilder.ConfigureServices((context, services) =>
            {
                //TBD
                services.AddTransient<IConfiguration>(sp =>
                {
                    var configurationBuilder = new ConfigurationBuilder();
                    configurationBuilder.SetBasePath(Path.Combine(Directory.GetCurrentDirectory(), "Settings"));
                    configurationBuilder.AddJsonFile("appsettings.json");
                    return configurationBuilder.Build();
                });
                //services.AddTransient<Settings.SettingsProvider>();
                services.AddSingleton<Settings.SettingsProvider>();
                services.AddTransient<DaData.Provider>();
                services.AddScoped<MainForm>();
                services.AddSingleton<VisitManager>();
                services.AddScoped<AddEditPatientForm>();
                services.AddScoped<RepositoryDB>();
                services.AddScoped<ApplicationDbContext>();
                services.AddScoped<ReportPatientsForm>();
                services.AddScoped<ReportDiseasesForm>();
                services.AddScoped<ReportVisitForm>();
            });

            var host = hostBuilder.Build();

            Application.Run(host.Services.GetRequiredService<MainForm>());
        }
    }
}