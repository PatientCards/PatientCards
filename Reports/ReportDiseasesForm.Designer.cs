﻿using PatientCards.Domain;

namespace PatientCards
{
    partial class ReportDiseasesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewPatientsWithDisease = new System.Windows.Forms.DataGridView();
            this.patientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.buttonPatientsWithDiseaseShow = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSelectDiseaseReport = new System.Windows.Forms.TextBox();
            this.checkBoxRegOnly = new System.Windows.Forms.CheckBox();
            this.Disease = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.secondNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDayDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsUnregistred = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatientsWithDisease)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewPatientsWithDisease
            // 
            this.dataGridViewPatientsWithDisease.AutoGenerateColumns = false;
            this.dataGridViewPatientsWithDisease.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPatientsWithDisease.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Disease,
            this.lastNameDataGridViewTextBoxColumn,
            this.firstNameDataGridViewTextBoxColumn,
            this.secondNameDataGridViewTextBoxColumn,
            this.birthDayDataGridViewTextBoxColumn,
            this.addressDataGridViewTextBoxColumn,
            this.IsUnregistred});
            this.dataGridViewPatientsWithDisease.DataSource = this.patientBindingSource;
            this.dataGridViewPatientsWithDisease.Location = new System.Drawing.Point(12, 87);
            this.dataGridViewPatientsWithDisease.Name = "dataGridViewPatientsWithDisease";
            this.dataGridViewPatientsWithDisease.ReadOnly = true;
            this.dataGridViewPatientsWithDisease.RowTemplate.Height = 25;
            this.dataGridViewPatientsWithDisease.Size = new System.Drawing.Size(949, 243);
            this.dataGridViewPatientsWithDisease.TabIndex = 0;
            // 
            // patientBindingSource
            // 
            this.patientBindingSource.DataSource = typeof(PatientCards.Domain.Patient);
            // 
            // buttonPatientsWithDiseaseShow
            // 
            this.buttonPatientsWithDiseaseShow.Location = new System.Drawing.Point(424, 25);
            this.buttonPatientsWithDiseaseShow.Name = "buttonPatientsWithDiseaseShow";
            this.buttonPatientsWithDiseaseShow.Size = new System.Drawing.Size(286, 23);
            this.buttonPatientsWithDiseaseShow.TabIndex = 1;
            this.buttonPatientsWithDiseaseShow.Text = "Показать пациентов с выбранным диагнозом";
            this.buttonPatientsWithDiseaseShow.UseVisualStyleBackColor = true;
            this.buttonPatientsWithDiseaseShow.Click += new System.EventHandler(this.buttonPatientsWithDiseaseShow_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Выберите диагноз";
            // 
            // textBoxSelectDiseaseReport
            // 
            this.textBoxSelectDiseaseReport.Location = new System.Drawing.Point(140, 25);
            this.textBoxSelectDiseaseReport.Name = "textBoxSelectDiseaseReport";
            this.textBoxSelectDiseaseReport.Size = new System.Drawing.Size(241, 23);
            this.textBoxSelectDiseaseReport.TabIndex = 3;
            this.textBoxSelectDiseaseReport.Click += new System.EventHandler(this.textBoxSelectDiseaseReport_Click);
            // 
            // checkBoxRegOnly
            // 
            this.checkBoxRegOnly.AutoSize = true;
            this.checkBoxRegOnly.Location = new System.Drawing.Point(12, 62);
            this.checkBoxRegOnly.Name = "checkBoxRegOnly";
            this.checkBoxRegOnly.Size = new System.Drawing.Size(230, 19);
            this.checkBoxRegOnly.TabIndex = 4;
            this.checkBoxRegOnly.Text = "Показать только состоящих на учёте";
            this.checkBoxRegOnly.UseVisualStyleBackColor = true;
            // 
            // Disease
            // 
            this.Disease.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Disease.DataPropertyName = "Disease";
            this.Disease.HeaderText = "Диагноз";
            this.Disease.Name = "Disease";
            this.Disease.ReadOnly = true;
            this.Disease.Width = 77;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "Фамилия";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.lastNameDataGridViewTextBoxColumn.Width = 83;
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "FirstName";
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "Имя";
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            this.firstNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.firstNameDataGridViewTextBoxColumn.Width = 56;
            // 
            // secondNameDataGridViewTextBoxColumn
            // 
            this.secondNameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.secondNameDataGridViewTextBoxColumn.DataPropertyName = "SecondName";
            this.secondNameDataGridViewTextBoxColumn.HeaderText = "Отчество";
            this.secondNameDataGridViewTextBoxColumn.Name = "secondNameDataGridViewTextBoxColumn";
            this.secondNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.secondNameDataGridViewTextBoxColumn.Width = 83;
            // 
            // birthDayDataGridViewTextBoxColumn
            // 
            this.birthDayDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.birthDayDataGridViewTextBoxColumn.DataPropertyName = "BirthDay";
            this.birthDayDataGridViewTextBoxColumn.HeaderText = "Дата рождения";
            this.birthDayDataGridViewTextBoxColumn.Name = "birthDayDataGridViewTextBoxColumn";
            this.birthDayDataGridViewTextBoxColumn.ReadOnly = true;
            this.birthDayDataGridViewTextBoxColumn.Width = 106;
            // 
            // addressDataGridViewTextBoxColumn
            // 
            this.addressDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.addressDataGridViewTextBoxColumn.DataPropertyName = "Address";
            this.addressDataGridViewTextBoxColumn.HeaderText = "Место жительства";
            this.addressDataGridViewTextBoxColumn.Name = "addressDataGridViewTextBoxColumn";
            this.addressDataGridViewTextBoxColumn.ReadOnly = true;
            this.addressDataGridViewTextBoxColumn.Width = 122;
            // 
            // IsUnregistred
            // 
            this.IsUnregistred.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.IsUnregistred.DataPropertyName = "IsUnregistred";
            this.IsUnregistred.HeaderText = "Снят с учета";
            this.IsUnregistred.Name = "IsUnregistred";
            this.IsUnregistred.ReadOnly = true;
            this.IsUnregistred.Width = 73;
            // 
            // ReportDiseasesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 450);
            this.Controls.Add(this.checkBoxRegOnly);
            this.Controls.Add(this.textBoxSelectDiseaseReport);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonPatientsWithDiseaseShow);
            this.Controls.Add(this.dataGridViewPatientsWithDisease);
            this.Name = "ReportDiseasesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор пациентов по диагнозу";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatientsWithDisease)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataGridView dataGridViewPatientsWithDisease;
        private Button buttonPatientsWithDiseaseShow;
        private Label label1;
        private TextBox textBoxSelectDiseaseReport;
        private BindingSource patientBindingSource;
        private CheckBox checkBoxRegOnly;
        private DataGridViewTextBoxColumn Disease;
        private DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn secondNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn birthDayDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn addressDataGridViewTextBoxColumn;
        private DataGridViewCheckBoxColumn IsUnregistred;
    }
}