﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Settings
{
    public class SettingsProvider
    {
        private const string CONNECTION_STRING = "CONNECTION_STRING";
        private const string DADATA_TOKEN = "DADATA_TOKEN";
        private const string DADATA_SECRET = "DADATA_SECRET";
        private readonly IConfiguration _configuration;

        public SettingsProvider(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public string GetConnectionString()
        {
            var value = _configuration[CONNECTION_STRING];
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException($"{nameof(CONNECTION_STRING)} is not found");
            }

            return value;
        }
        public string GetDaDataToken()
        {
            var value = _configuration[DADATA_TOKEN];
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException($"{nameof(DADATA_TOKEN)} is not found");
            }
            return value;
        }
        public string GetDaDataSecret()
        {
            var value = _configuration[DADATA_SECRET];
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentNullException($"{nameof(DADATA_SECRET)} is not found");
            }
            return value;
        }
    }

}



