﻿using Dadata;
using PatientCards.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.DaData
{
    public class Provider
    {
        //private readonly Settings.SettingsProvider _settingsProvider;
        private readonly SuggestClientAsync _daDataApi;

        public Provider(SettingsProvider settingsProvider)
        {
            //_settingsProvider = settingsProvider;
            _daDataApi = new SuggestClientAsync(settingsProvider.GetDaDataToken());
        }

        public async Task<string[]> GetAddressFromDaData(string input)
        {
            //var token = "be78f5f73d5ee0851367d2b5c8ceec433dbdd482";
            //var secret = "e7ea40a3f13c146d63fc2b503dd760e5274a3678";
            //var api = new SuggestClientAsync(token);
            try
            {
                var response = await _daDataApi.SuggestAddress(input);
                return response.suggestions.Select(s => s.value).ToArray();
            }
            catch
            {
                throw new Exception("Something is wrong with DaData");
            }
        }
    }
}
