﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


//[assembly: AssemblyVersion("1.2.3.4")]
//[assembly: AssemblyFileVersion("6.6.6.6")]
//[assembly: AssemblyInformationalVersion("So many numbers!")]

namespace PatientCards.Settings
{
    public class VersionNumber
    {
        //public const string VERSION_NUMBER = "v1.0.1";
        public static string GetInformationalVersion(Assembly assembly)
        {
            return FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
        }
    }
}
