﻿namespace PatientCards
{
    partial class AddEditPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxAddPatient = new System.Windows.Forms.GroupBox();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.comboBoxAddress = new System.Windows.Forms.ComboBox();
            this.textBoxDisease = new System.Windows.Forms.TextBox();
            this.labelDisease = new System.Windows.Forms.Label();
            this.labelUnreg = new System.Windows.Forms.Label();
            this.dateTimePickerUnreg = new System.Windows.Forms.DateTimePicker();
            this.checkBoxUnreg = new System.Windows.Forms.CheckBox();
            this.dateTimePickerReg = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.dateTimePickerBirthDay = new System.Windows.Forms.DateTimePicker();
            this.textBoxSecondName = new System.Windows.Forms.TextBox();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.labelAddress = new System.Windows.Forms.Label();
            this.labelBirthDay = new System.Windows.Forms.Label();
            this.labelSecondName = new System.Windows.Forms.Label();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.labelLastName = new System.Windows.Forms.Label();
            this.groupBoxAddPatient.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxAddPatient
            // 
            this.groupBoxAddPatient.Controls.Add(this.buttonRemove);
            this.groupBoxAddPatient.Controls.Add(this.comboBoxAddress);
            this.groupBoxAddPatient.Controls.Add(this.textBoxDisease);
            this.groupBoxAddPatient.Controls.Add(this.labelDisease);
            this.groupBoxAddPatient.Controls.Add(this.labelUnreg);
            this.groupBoxAddPatient.Controls.Add(this.dateTimePickerUnreg);
            this.groupBoxAddPatient.Controls.Add(this.checkBoxUnreg);
            this.groupBoxAddPatient.Controls.Add(this.dateTimePickerReg);
            this.groupBoxAddPatient.Controls.Add(this.label1);
            this.groupBoxAddPatient.Controls.Add(this.buttonSave);
            this.groupBoxAddPatient.Controls.Add(this.dateTimePickerBirthDay);
            this.groupBoxAddPatient.Controls.Add(this.textBoxSecondName);
            this.groupBoxAddPatient.Controls.Add(this.textBoxFirstName);
            this.groupBoxAddPatient.Controls.Add(this.textBoxLastName);
            this.groupBoxAddPatient.Controls.Add(this.labelAddress);
            this.groupBoxAddPatient.Controls.Add(this.labelBirthDay);
            this.groupBoxAddPatient.Controls.Add(this.labelSecondName);
            this.groupBoxAddPatient.Controls.Add(this.labelFirstName);
            this.groupBoxAddPatient.Controls.Add(this.labelLastName);
            this.groupBoxAddPatient.Location = new System.Drawing.Point(12, 3);
            this.groupBoxAddPatient.Name = "groupBoxAddPatient";
            this.groupBoxAddPatient.Size = new System.Drawing.Size(369, 466);
            this.groupBoxAddPatient.TabIndex = 1;
            this.groupBoxAddPatient.TabStop = false;
            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(204, 427);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(152, 23);
            this.buttonRemove.TabIndex = 19;
            this.buttonRemove.Text = "Удалить пациента";
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // comboBoxAddress
            // 
            this.comboBoxAddress.FormattingEnabled = true;
            this.comboBoxAddress.Location = new System.Drawing.Point(10, 193);
            this.comboBoxAddress.Name = "comboBoxAddress";
            this.comboBoxAddress.Size = new System.Drawing.Size(346, 23);
            this.comboBoxAddress.TabIndex = 18;
            this.comboBoxAddress.TextChanged += new System.EventHandler(this.comboBoxAddress_TextChanged);
            // 
            // textBoxDisease
            // 
            this.textBoxDisease.Location = new System.Drawing.Point(128, 265);
            this.textBoxDisease.Name = "textBoxDisease";
            this.textBoxDisease.Size = new System.Drawing.Size(228, 23);
            this.textBoxDisease.TabIndex = 17;
            // 
            // labelDisease
            // 
            this.labelDisease.AutoSize = true;
            this.labelDisease.Location = new System.Drawing.Point(10, 268);
            this.labelDisease.Name = "labelDisease";
            this.labelDisease.Size = new System.Drawing.Size(52, 15);
            this.labelDisease.TabIndex = 16;
            this.labelDisease.Text = "Диагноз";
            // 
            // labelUnreg
            // 
            this.labelUnreg.AutoSize = true;
            this.labelUnreg.Location = new System.Drawing.Point(10, 355);
            this.labelUnreg.Name = "labelUnreg";
            this.labelUnreg.Size = new System.Drawing.Size(114, 15);
            this.labelUnreg.TabIndex = 15;
            this.labelUnreg.Text = "Дата снятия с учёта";
            this.labelUnreg.Visible = false;
            // 
            // dateTimePickerUnreg
            // 
            this.dateTimePickerUnreg.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerUnreg.Location = new System.Drawing.Point(156, 349);
            this.dateTimePickerUnreg.Name = "dateTimePickerUnreg";
            this.dateTimePickerUnreg.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerUnreg.TabIndex = 14;
            this.dateTimePickerUnreg.Visible = false;
            // 
            // checkBoxUnreg
            // 
            this.checkBoxUnreg.AutoSize = true;
            this.checkBoxUnreg.Location = new System.Drawing.Point(10, 333);
            this.checkBoxUnreg.Name = "checkBoxUnreg";
            this.checkBoxUnreg.Size = new System.Drawing.Size(94, 19);
            this.checkBoxUnreg.TabIndex = 13;
            this.checkBoxUnreg.Text = "Снят с учёта";
            this.checkBoxUnreg.UseVisualStyleBackColor = true;
            this.checkBoxUnreg.CheckedChanged += new System.EventHandler(this.checkBoxUnreg_CheckedChanged);
            // 
            // dateTimePickerReg
            // 
            this.dateTimePickerReg.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerReg.Location = new System.Drawing.Point(158, 228);
            this.dateTimePickerReg.Name = "dateTimePickerReg";
            this.dateTimePickerReg.Size = new System.Drawing.Size(198, 23);
            this.dateTimePickerReg.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 15);
            this.label1.TabIndex = 11;
            this.label1.Text = "Дата постановки на учет";
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(10, 427);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(158, 23);
            this.buttonSave.TabIndex = 10;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // dateTimePickerBirthDay
            // 
            this.dateTimePickerBirthDay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerBirthDay.Location = new System.Drawing.Point(158, 135);
            this.dateTimePickerBirthDay.Name = "dateTimePickerBirthDay";
            this.dateTimePickerBirthDay.Size = new System.Drawing.Size(198, 23);
            this.dateTimePickerBirthDay.TabIndex = 9;
            // 
            // textBoxSecondName
            // 
            this.textBoxSecondName.Location = new System.Drawing.Point(128, 103);
            this.textBoxSecondName.Name = "textBoxSecondName";
            this.textBoxSecondName.Size = new System.Drawing.Size(228, 23);
            this.textBoxSecondName.TabIndex = 7;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Location = new System.Drawing.Point(128, 71);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(228, 23);
            this.textBoxFirstName.TabIndex = 6;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(128, 40);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(228, 23);
            this.textBoxLastName.TabIndex = 5;
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Location = new System.Drawing.Point(10, 172);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(112, 15);
            this.labelAddress.TabIndex = 4;
            this.labelAddress.Text = "Адрес проживания";
            // 
            // labelBirthDay
            // 
            this.labelBirthDay.AutoSize = true;
            this.labelBirthDay.Location = new System.Drawing.Point(10, 141);
            this.labelBirthDay.Name = "labelBirthDay";
            this.labelBirthDay.Size = new System.Drawing.Size(90, 15);
            this.labelBirthDay.TabIndex = 3;
            this.labelBirthDay.Text = "Дата рождения";
            // 
            // labelSecondName
            // 
            this.labelSecondName.AutoSize = true;
            this.labelSecondName.Location = new System.Drawing.Point(10, 106);
            this.labelSecondName.Name = "labelSecondName";
            this.labelSecondName.Size = new System.Drawing.Size(58, 15);
            this.labelSecondName.TabIndex = 2;
            this.labelSecondName.Text = "Отчество";
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(10, 74);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(31, 15);
            this.labelFirstName.TabIndex = 1;
            this.labelFirstName.Text = "Имя";
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(10, 43);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(58, 15);
            this.labelLastName.TabIndex = 0;
            this.labelLastName.Text = "Фамилия";
            // 
            // AddEditPatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 501);
            this.Controls.Add(this.groupBoxAddPatient);
            this.Name = "AddEditPatientForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Карта пациента";
            this.Shown += new System.EventHandler(this.AddEditPatientForm_Shown);
            this.groupBoxAddPatient.ResumeLayout(false);
            this.groupBoxAddPatient.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox groupBoxAddPatient;
        private DateTimePicker dateTimePickerReg;
        private Label label1;
        private Button buttonSave;
        private DateTimePicker dateTimePickerBirthDay;
        private TextBox textBoxSecondName;
        private TextBox textBoxFirstName;
        private TextBox textBoxLastName;
        private Label labelAddress;
        private Label labelBirthDay;
        private Label labelSecondName;
        private Label labelFirstName;
        private Label labelLastName;
        private CheckBox checkBoxUnreg;
        private DateTimePicker dateTimePickerUnreg;
        private Label labelUnreg;
        private Label labelDisease;
        private TextBox textBoxDisease;
        private ComboBox comboBoxAddress;
        private Button buttonRemove;
    }
}