﻿using PatientCards.Domain;

namespace PatientCards
{
    partial class ReportVisitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonReportVisitShowVisits = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxReportVisitSelectPatient = new System.Windows.Forms.TextBox();
            this.dataGridViewVisits = new System.Windows.Forms.DataGridView();
            this.VisitDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.visitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.patientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.diseaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVisits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.visitBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diseaseBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonReportVisitShowVisits
            // 
            this.buttonReportVisitShowVisits.Location = new System.Drawing.Point(472, 32);
            this.buttonReportVisitShowVisits.Name = "buttonReportVisitShowVisits";
            this.buttonReportVisitShowVisits.Size = new System.Drawing.Size(204, 23);
            this.buttonReportVisitShowVisits.TabIndex = 0;
            this.buttonReportVisitShowVisits.Text = "Показать все посещения";
            this.buttonReportVisitShowVisits.UseVisualStyleBackColor = true;
            this.buttonReportVisitShowVisits.Click += new System.EventHandler(this.buttonReportVisitShowVisits_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Выберите пациента";
            // 
            // textBoxReportVisitSelectPatient
            // 
            this.textBoxReportVisitSelectPatient.Location = new System.Drawing.Point(133, 32);
            this.textBoxReportVisitSelectPatient.Name = "textBoxReportVisitSelectPatient";
            this.textBoxReportVisitSelectPatient.Size = new System.Drawing.Size(316, 23);
            this.textBoxReportVisitSelectPatient.TabIndex = 2;
            this.textBoxReportVisitSelectPatient.Enter += new System.EventHandler(this.textBoxReportVisitSelectPatient_Enter);
            // 
            // dataGridViewVisits
            // 
            this.dataGridViewVisits.AutoGenerateColumns = false;
            this.dataGridViewVisits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewVisits.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.VisitDate,
            this.Comment});
            this.dataGridViewVisits.DataSource = this.visitBindingSource;
            this.dataGridViewVisits.Location = new System.Drawing.Point(12, 80);
            this.dataGridViewVisits.Name = "dataGridViewVisits";
            this.dataGridViewVisits.ReadOnly = true;
            this.dataGridViewVisits.RowTemplate.Height = 25;
            this.dataGridViewVisits.Size = new System.Drawing.Size(744, 358);
            this.dataGridViewVisits.TabIndex = 3;
            // 
            // VisitDate
            // 
            this.VisitDate.DataPropertyName = "VisitDate";
            this.VisitDate.HeaderText = "Дата посещения";
            this.VisitDate.Name = "VisitDate";
            this.VisitDate.ReadOnly = true;
            // 
            // Comment
            // 
            this.Comment.DataPropertyName = "Comment";
            this.Comment.HeaderText = "Комментарии";
            this.Comment.Name = "Comment";
            this.Comment.ReadOnly = true;
            this.Comment.Width = 600;
            // 
            // visitBindingSource
            // 
            this.visitBindingSource.DataSource = typeof(PatientCards.Domain.Visit);
            // 
            // patientBindingSource
            // 
            this.patientBindingSource.DataSource = typeof(PatientCards.Domain.Patient);
            // 
            // diseaseBindingSource
            // 
            this.diseaseBindingSource.DataSource = typeof(PatientCards.Domain.Disease);
            // 
            // ReportVisitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 450);
            this.Controls.Add(this.dataGridViewVisits);
            this.Controls.Add(this.textBoxReportVisitSelectPatient);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonReportVisitShowVisits);
            this.Name = "ReportVisitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Посещения пациентов";
            this.Shown += new System.EventHandler(this.ReportVisitForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewVisits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.visitBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diseaseBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button buttonReportVisitShowVisits;
        private Label label1;
        private TextBox textBoxReportVisitSelectPatient;
        private DataGridView dataGridViewVisits;
        private DataGridViewTextBoxColumn VisitDate;
        private DataGridViewTextBoxColumn Comment;
        private BindingSource visitBindingSource;
        private BindingSource patientBindingSource;
        private BindingSource diseaseBindingSource;
    }
}