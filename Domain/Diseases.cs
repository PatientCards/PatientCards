﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Domain
{
    public class Disease
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public List<Patient> Patients { get; set; } = new List<Patient>();

        public Disease(string name)
        {
            Name = name;
        }
    }
}
