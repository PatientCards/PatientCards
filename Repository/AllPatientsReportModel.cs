﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Repository
{
    public class AllPatientsReportModel
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime BirthDay { get; set; }
        public string? Address { get; set; }
        public string? Disease { get; set; }
        public DateTime? regDate { get; set; } //= DateTime.MinValue;
        public DateTime? unregDate { get; set; } //= DateTime.MinValue;
        public bool IsUnregistred { get; set; }
    }
}
