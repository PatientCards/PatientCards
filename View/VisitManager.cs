﻿using PatientCards.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Forms
{
    public class VisitManager
    {
        public Patient? Patient { get; private set; }
        public void SetPatient(Patient? patient)
        {
            Patient = patient;
        }
    }
}
