﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Domain
{
    public class Visit
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime VisitDate { get; private set; }

        public int PatientId { get; set; }
        public Patient? Patient { get; set; }
        public string? Comment { get; private set; }

        public Visit(DateTime visitDate)
        {
            VisitDate = visitDate;
        }
        public void SetComment(string comment)
        {
            Comment = comment;
        }
    }
}
