﻿using Microsoft.EntityFrameworkCore;
using PatientCards.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using PatientCards.Settings;
using PatientCards.View;

namespace PatientCards.Repository
{
    public class RepositoryDB
    {
        private readonly ApplicationDbContext _db;
        
        public RepositoryDB(ApplicationDbContext dbContext)
        { 
            _db = dbContext;
        }
        public string[] GetPatientFullNames()
        {
            return _db.Patients.Select(p => p.LastName + " " + p.FirstName + " " + p.SecondName + " " + p.BirthDay.ToShortDateString()).ToArray();
        }

        public int? CreatePatient(string diseaseName,
            string lastName,
            string firstName,
            string secondName,
            DateTime birthDay,
            string address,
            DateTime regDate,
            bool isUnregistred,
            DateTime? unregDate)
        {
                var disease = _db.Diseases.Include(p => p.Patients).Where(d => d.Name == diseaseName).FirstOrDefault();
                //If disease doesn't exist, add it into database
                if (disease == null && string.IsNullOrWhiteSpace(diseaseName) == false)
                {
                    disease = new Disease(diseaseName);// { Name = diseaseName };
                    //db.Diseases.Add(disease);
                }
            if (string.IsNullOrWhiteSpace(lastName) == false
                && string.IsNullOrWhiteSpace(firstName) == false
                && string.IsNullOrWhiteSpace(secondName) == false
                && CheckExistsByName(lastName, firstName, secondName, birthDay) == false)    //to avoid creation of patients with the same full name)
            {
                Patient patient_db = new Patient(lastName, firstName, secondName, birthDay);

                patient_db.SetAddress(address);

                patient_db.SetRegDate(regDate);
                patient_db.SetDisease(disease);
                patient_db.SetUnregistered(isUnregistred);
                patient_db.SetUnregDate(unregDate);

                _db.Patients.Add(patient_db);
                _db.SaveChanges();
                return patient_db.Id;
            }
            return null;
        }

        public void UpdatePatient(int patientId,
            string diseaseName,
            string lastName,
            string firstName,
            string secondName,
            DateTime birthDay,
            string address,
            DateTime regDate,
            bool isUnregistred,
            DateTime? unregDate)
        {
            var disease = _db.Diseases.Include(p => p.Patients).Where(d => d.Name == diseaseName).FirstOrDefault();
            //If disease doesn't exist, add it into database
            if (disease == null && string.IsNullOrWhiteSpace(diseaseName) == false)
            {
                disease = new Disease(diseaseName);
            }

            Patient patient_db = _db.Patients.Where(p => p.Id == patientId).FirstOrDefault();

            if (patient_db != null)
            {
                patient_db.LastName = lastName;
                patient_db.FirstName = firstName;
                patient_db.SecondName = secondName;
                patient_db.BirthDay = birthDay;
                patient_db.Address = address;
                patient_db.regDate = regDate;
                patient_db.Disease = disease;
                patient_db.DiseaseId = disease?.Id;
                patient_db.IsUnregistred = isUnregistred;
                patient_db.unregDate = unregDate;
                _db.Patients.Update(patient_db);
                _db.SaveChanges();
            }
        }

        public void RemovePatient(int patientId)
        {
            var patient = _db.Patients.Where(p => p.Id == patientId).FirstOrDefault();
            if (patient != null)
            {
                _db.Patients.Remove(patient);
                _db.SaveChanges();
            }
        }

        public bool CheckExistsById(int patientId)
        {
            if (_db.Patients.Where(p => p.Id == patientId).FirstOrDefault() == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool CheckExistsByName(string lastName, string firstName, string secondName, DateTime birthDay)
        {
            if (_db.Patients
                .Where(p => p.LastName == lastName)
                .Where(p => p.FirstName == firstName)
                .Where(p => p.SecondName == secondName)
                .FirstOrDefault() == null
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Patient? GetPatientById(int? patientId)
        {
            return _db.Patients.Where(p => p.Id == patientId).FirstOrDefault();
        }

        public void AddVisit(
            int patientId,
            string comment,
            DateTime visitDate)
        {
            Visit visit = new Visit(visitDate);
            visit.SetComment(comment);
            //visit.VisitDate = visitDate;
            _db.Visits.Add(visit);
            visit.Patient = _db.Patients.Where(p => p.Id == patientId).FirstOrDefault();
            _db.SaveChanges();
        }

        public string[] GetStringsForAutoCompletion(AutoCompletionType type)
        {
            string[] str = Array.Empty<string>();

            switch (type)
            {
                case AutoCompletionType.LastName:
                    str = _db.Patients.Select(p => p.LastName).ToArray();
                    break;
                case AutoCompletionType.FirstName:
                    str = _db.Patients.Select(p => p.FirstName).ToArray();
                    break;
                case AutoCompletionType.SecondName:
                    str = _db.Patients.Select(p => p.SecondName).ToArray();
                    break;
                case AutoCompletionType.Disease:
                    str = _db.Diseases.Select(d => d.Name).ToArray();
                    break;
                case AutoCompletionType.FullName:
                    str = GetPatientFullNames();
                    break;
            }
            return str;
        }

        public List<ReportDiseaseModel> GetPatientsWithDisease(string diseaseName, bool regOnly)
        {
            Disease? disease = _db.Diseases.Where(d => d.Name == diseaseName).FirstOrDefault();
            if (disease != null)
            {
                if (regOnly)
                {
                    return _db.Patients.Include(d => d.Disease).Select(p => new ReportDiseaseModel
                    {
                        Disease = p.Disease.Name,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        SecondName = p.SecondName,
                        BirthDay = p.BirthDay,
                        Address = p.Address,
                        IsUnregistred = p.IsUnregistred
                    })
                        .Where(p => p.Disease == diseaseName)
                        .Where(p => p.IsUnregistred == false)
                        .OrderBy(p => p.LastName)
                        .ThenBy(p => p.FirstName)
                        .ThenBy(p => p.SecondName)
                        .ToList();
                }
                else
                {
                    return _db.Patients.Include(d => d.Disease).Select(p => new ReportDiseaseModel
                    {
                        Disease = p.Disease.Name,
                        LastName = p.LastName,
                        FirstName = p.FirstName,
                        SecondName = p.SecondName,
                        BirthDay = p.BirthDay,
                        Address = p.Address,
                        IsUnregistred = p.IsUnregistred
                    })
                        .Where(p => p.Disease == diseaseName)
                        .OrderBy(p => p.LastName)
                        .ThenBy(p => p.FirstName)
                        .ThenBy(p => p.SecondName)
                        .ToList();
                }
            }
            return new List<ReportDiseaseModel>();
        }

        public List<ReportVisitModel> GetVisitsForPatient(string patientFullName)
        {
            Patient? patient = FindPatient(patientFullName);
            if (patient != null)
            {
                var value = _db.Visits
                    .Select(v => new ReportVisitModel { VisitDate = v.VisitDate, Comment = v.Comment, PatientId = v.PatientId })
                    .Where(v => v.PatientId == patient.Id)
                    .ToList();
                return value;
            }
            return new List<ReportVisitModel>();
        }

        public List<AllPatientsReportModel> GetAllPatients()
        {
            var result = _db.Patients
                //.AsNoTracking()
                .Include(d => d.Disease)
                .Select(p => new AllPatientsReportModel
                {
                    Id = p.Id,
                    LastName = p.LastName,
                    FirstName = p.FirstName,
                    SecondName = p.SecondName,
                    BirthDay = p.BirthDay,
                    Address = p.Address,
                    regDate = p.regDate,
                    unregDate = p.unregDate,
                    IsUnregistred = p.IsUnregistred,
                    Disease = p.Disease == null ? String.Empty : p.Disease.Name
                })
                .OrderBy(p => p.LastName)
                .ThenBy(p => p.FirstName)
                .ThenBy(p => p.SecondName)
                .ToList();
            return result;
        }

        public Patient? FindPatient(string str)
        {
            try
            {
                string[] pname = str.Split(new char[] { ' ' });
                string lastname = pname[0].ToString();
                string firstname = pname[1].ToString();
                string secondname = pname[2].ToString();
                DateTime.TryParse(pname[3], out DateTime birthday);

                return _db.Patients.Include(d => d.Disease)
                    .Where(p => p.LastName == lastname)
                    .Where(p => p.FirstName == firstname)
                    .Where(p => p.SecondName == secondname)
                    .Where(p => p.BirthDay == birthday).FirstOrDefault();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Проверьте правильность введенных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

    }
}
