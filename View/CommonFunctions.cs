﻿using Microsoft.EntityFrameworkCore;
using PatientCards.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dadata;
using Dadata.Model;
using PatientCards.Repository;

namespace PatientCards.View
{
    public enum AutoCompletionType
    {
        LastName,
        FirstName,
        SecondName,
        Disease,
        FullName
    }

    public static class TextBoxExtention
    {
        public static void CreateAutoCompletion(this TextBox textBox, string[] dictionary)
        {
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            source.AddRange(dictionary);

            textBox.AutoCompleteCustomSource = source;
            textBox.AutoCompleteMode = AutoCompleteMode.Suggest;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;

        }
    }
}
