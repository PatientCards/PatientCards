﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Domain
{
    public class Patient
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime BirthDay { get; set; }
        public string? Address { get; set; }
        public int? DiseaseId { get; set; }
        public Disease? Disease { get; set; }
        //public List<Disease> Diseases { get; set; } = new();

        //public int VisitId  { get; set; }
        public List<Visit> Visits { get; set; } = new();
        public DateTime? regDate { get; set; } //= DateTime.MinValue;
        public DateTime? unregDate { get; set; } //= DateTime.MinValue;
        public bool IsUnregistred { get; set; }

        public string GetFullName()
        {
            return LastName + " " + FirstName + " " + SecondName + " " + BirthDay.ToShortDateString();
        }

        public Patient(string lastName, string firstName, string secondName, DateTime birthDay)
        {
            LastName = lastName;
            FirstName = firstName;
            SecondName = secondName;
            BirthDay = birthDay;
        }

        public void SetAddress (string address)
        {
            if(String.IsNullOrWhiteSpace(address))
                return;
            if (string.Equals(address, Address, StringComparison.CurrentCultureIgnoreCase))
                return;
            Address = address;
        }

        public void SetRegDate(DateTime date)
        {
            regDate = date;
        }
        public void SetUnregDate(DateTime? date)
        {
            unregDate = date;
        }
        public void SetUnregistered(bool isunreg)
        {
            IsUnregistred = isunreg;
        }

        public void SetDisease(Disease disease)
        {
            Disease = disease;
        }
    }
}
