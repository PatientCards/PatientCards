﻿using Microsoft.EntityFrameworkCore;
using PatientCards.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientCards.Repository;
using PatientCards.Forms;
using PatientCards.DaData;
using PatientCards.View;

namespace PatientCards
{
    public partial class AddEditPatientForm : Form
    {
        private readonly VisitManager _visitManager;
        private readonly RepositoryDB _repositoryDB;
        private readonly DaData.Provider _daDataProvider;

        public AddEditPatientForm(VisitManager visitManager, RepositoryDB repositoryDB, DaData.Provider daDataProvider)
        {
            InitializeComponent();
            _visitManager = visitManager;
            _repositoryDB = repositoryDB;
            _daDataProvider = daDataProvider;
        }

        public void LoadPatientFromVisitManager()
        {
            if (_visitManager.Patient != null)
            {
                textBoxLastName.Text = _visitManager.Patient.LastName;
                textBoxFirstName.Text = _visitManager.Patient.FirstName;
                textBoxSecondName.Text = _visitManager.Patient.SecondName;
                comboBoxAddress.Items.Clear();
                comboBoxAddress.Text = _visitManager.Patient.Address;
                if (_visitManager.Patient.Disease != null)
                    textBoxDisease.Text = _visitManager.Patient.Disease.Name;

                dateTimePickerBirthDay.Value = _visitManager.Patient.BirthDay;
                dateTimePickerReg.Value = (DateTime)_visitManager.Patient.regDate;
                if (_visitManager.Patient.unregDate != null)
                {
                    dateTimePickerUnreg.Value = (DateTime)_visitManager.Patient.unregDate;
                }

                checkBoxUnreg.Checked = _visitManager.Patient.IsUnregistred;
            }
            else
            {
                textBoxLastName.Text = string.Empty;
                textBoxFirstName.Text = string.Empty;
                textBoxSecondName.Text = string.Empty;
                comboBoxAddress.Items.Clear();
                comboBoxAddress.Text = string.Empty;
                textBoxDisease.Text = string.Empty;

                dateTimePickerBirthDay.Value = DateTime.Now.Date;
                dateTimePickerReg.Value = DateTime.Now.Date;
                dateTimePickerUnreg.Value = DateTime.Now.Date;
                checkBoxUnreg.Checked = false;
            }
        }
        public new MainForm Owner { get; set; }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            int? patientId;
            
            DateTime? unregDate = null;
            if (checkBoxUnreg.Checked)
            {
                unregDate = dateTimePickerUnreg.Value.Date;
            }

            if (_visitManager.Patient != null)
            {
                _repositoryDB.UpdatePatient(_visitManager.Patient.Id,
                    textBoxDisease.Text,
                    textBoxLastName.Text,
                    textBoxFirstName.Text,
                    textBoxSecondName.Text,
                    dateTimePickerBirthDay.Value.Date,
                    comboBoxAddress.Text,
                    dateTimePickerReg.Value.Date,
                    checkBoxUnreg.Checked,
                    unregDate);
                patientId = _visitManager.Patient.Id;
            }
            else
            {
                patientId = _repositoryDB.CreatePatient(textBoxDisease.Text,
                textBoxLastName.Text,
                textBoxFirstName.Text,
                textBoxSecondName.Text,
                dateTimePickerBirthDay.Value.Date,
                comboBoxAddress.Text,
                dateTimePickerReg.Value.Date,
                checkBoxUnreg.Checked,
                unregDate);

                if (patientId != null)
                    MessageBox.Show($"Пациент создан", "Создание пациента", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            //fill the parent form with new data
            if (patientId != null)
            {
                //MainForm _mainForm = (MainForm)this.Owner; 
                //_mainForm.textBoxSelectPatient.Text = RepositoryDB.GetPatientById(patientId)!.GetFullName();
                Owner.textBoxSelectPatient.Text = _repositoryDB.GetPatientById(patientId)!.GetFullName();
            }
            
            Close();
        }

        private void checkBoxUnreg_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxUnreg.Checked)
            {
                dateTimePickerUnreg.Visible = true;
                labelUnreg.Visible = true;
            }
            else
            {
                dateTimePickerUnreg.Visible = false;
                labelUnreg.Visible = false;
            }
        }

        private void AddEditPatientForm_Shown(object sender, EventArgs e)
        {
            textBoxLastName.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.LastName));            
            textBoxFirstName.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.FirstName));
            textBoxSecondName.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.SecondName));
            textBoxDisease.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.Disease));

            //CommonFunctions.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.LastName), textBoxLastName);
            //CommonFunctions.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.FirstName), textBoxFirstName);
            //CommonFunctions.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.SecondName), textBoxSecondName);
            //CommonFunctions.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.Disease), textBoxDisease);

            if (_visitManager.Patient == null)
                buttonRemove.Enabled = false;
            else buttonRemove.Enabled = true;
        }

        private async void comboBoxAddress_TextChanged(object sender, EventArgs e)
        {
            if (comboBoxAddress.Text.Length >= 3)
            {
                //remove all before selected item
                while (comboBoxAddress.SelectedIndex > 0)
                    comboBoxAddress.Items.RemoveAt(0);
                //remove all after selected item
                while (comboBoxAddress.Items.Count > 1)
                    comboBoxAddress.Items.RemoveAt(1);
                comboBoxAddress.Items.AddRange(await _daDataProvider.GetAddressFromDaData(comboBoxAddress.Text));
                //comboBoxAddress.DroppedDown = true;

                //string str = comboBoxAddress.Text;
                //while (comboBoxAddress.Items.Count > 0)
                //{
                //    comboBoxAddress.Items.RemoveAt(0);
                //}
                //comboBoxAddress.Text = str;
                //comboBoxAddress.Items.AddRange(await _daDataProvider.GetAddressFromDaData(str));
            }
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show($"Вы точно хотите удалить пациента {_visitManager.Patient.GetFullName()}?", "Удаление пациента", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                if (_visitManager.Patient != null)
                    _repositoryDB.RemovePatient(_visitManager.Patient.Id);
                if (_repositoryDB.CheckExistsById(_visitManager.Patient.Id))
                    MessageBox.Show($"Пациент удалён", "Удаление пациента", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
        }
    }
}
