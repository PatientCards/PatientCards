﻿namespace PatientCards
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonEditPatient = new System.Windows.Forms.Button();
            this.groupBoxVisit = new System.Windows.Forms.GroupBox();
            this.buttonAddPatient = new System.Windows.Forms.Button();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.labelComment = new System.Windows.Forms.Label();
            this.textBoxSelectPatient = new System.Windows.Forms.TextBox();
            this.labelVisitDate = new System.Windows.Forms.Label();
            this.labelSelectPatient = new System.Windows.Forms.Label();
            this.dateTimePickerVisitDate = new System.Windows.Forms.DateTimePicker();
            this.buttonAddVisit = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выгрузкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allPatientsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allDiseasesReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visitsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxVisit.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonEditPatient
            // 
            this.buttonEditPatient.Location = new System.Drawing.Point(6, 90);
            this.buttonEditPatient.Name = "buttonEditPatient";
            this.buttonEditPatient.Size = new System.Drawing.Size(178, 23);
            this.buttonEditPatient.TabIndex = 10;
            this.buttonEditPatient.Text = "Редактировать пациента";
            this.buttonEditPatient.UseVisualStyleBackColor = true;
            this.buttonEditPatient.Click += new System.EventHandler(this.buttonEditPatient_Click);
            // 
            // groupBoxVisit
            // 
            this.groupBoxVisit.Controls.Add(this.buttonAddPatient);
            this.groupBoxVisit.Controls.Add(this.textBoxComment);
            this.groupBoxVisit.Controls.Add(this.labelComment);
            this.groupBoxVisit.Controls.Add(this.buttonEditPatient);
            this.groupBoxVisit.Controls.Add(this.textBoxSelectPatient);
            this.groupBoxVisit.Controls.Add(this.labelVisitDate);
            this.groupBoxVisit.Controls.Add(this.labelSelectPatient);
            this.groupBoxVisit.Controls.Add(this.dateTimePickerVisitDate);
            this.groupBoxVisit.Controls.Add(this.buttonAddVisit);
            this.groupBoxVisit.Location = new System.Drawing.Point(12, 45);
            this.groupBoxVisit.Name = "groupBoxVisit";
            this.groupBoxVisit.Size = new System.Drawing.Size(421, 307);
            this.groupBoxVisit.TabIndex = 2;
            this.groupBoxVisit.TabStop = false;
            this.groupBoxVisit.Text = "Добавление посещения";
            // 
            // buttonAddPatient
            // 
            this.buttonAddPatient.Location = new System.Drawing.Point(231, 90);
            this.buttonAddPatient.Name = "buttonAddPatient";
            this.buttonAddPatient.Size = new System.Drawing.Size(182, 23);
            this.buttonAddPatient.TabIndex = 11;
            this.buttonAddPatient.Text = "Добавить нового пациента";
            this.buttonAddPatient.UseVisualStyleBackColor = true;
            this.buttonAddPatient.Click += new System.EventHandler(this.buttonAddPatient_Click);
            // 
            // textBoxComment
            // 
            this.textBoxComment.Location = new System.Drawing.Point(6, 225);
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(407, 23);
            this.textBoxComment.TabIndex = 7;
            // 
            // labelComment
            // 
            this.labelComment.AutoSize = true;
            this.labelComment.Location = new System.Drawing.Point(6, 207);
            this.labelComment.Name = "labelComment";
            this.labelComment.Size = new System.Drawing.Size(217, 15);
            this.labelComment.TabIndex = 6;
            this.labelComment.Text = "Комментарии к данному посещению:";
            // 
            // textBoxSelectPatient
            // 
            this.textBoxSelectPatient.Location = new System.Drawing.Point(6, 61);
            this.textBoxSelectPatient.Name = "textBoxSelectPatient";
            this.textBoxSelectPatient.Size = new System.Drawing.Size(407, 23);
            this.textBoxSelectPatient.TabIndex = 5;
            this.textBoxSelectPatient.Click += new System.EventHandler(this.textBoxSelectPatient_Click);
            // 
            // labelVisitDate
            // 
            this.labelVisitDate.AutoSize = true;
            this.labelVisitDate.Location = new System.Drawing.Point(6, 147);
            this.labelVisitDate.Name = "labelVisitDate";
            this.labelVisitDate.Size = new System.Drawing.Size(98, 15);
            this.labelVisitDate.TabIndex = 3;
            this.labelVisitDate.Text = "Дата посещения";
            // 
            // labelSelectPatient
            // 
            this.labelSelectPatient.AutoSize = true;
            this.labelSelectPatient.Location = new System.Drawing.Point(6, 43);
            this.labelSelectPatient.Name = "labelSelectPatient";
            this.labelSelectPatient.Size = new System.Drawing.Size(407, 15);
            this.labelSelectPatient.TabIndex = 2;
            this.labelSelectPatient.Text = "Выберите пациента (например, Иванова Снежана Денисовна 01.01.2000)";
            // 
            // dateTimePickerVisitDate
            // 
            this.dateTimePickerVisitDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerVisitDate.Location = new System.Drawing.Point(6, 165);
            this.dateTimePickerVisitDate.Name = "dateTimePickerVisitDate";
            this.dateTimePickerVisitDate.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerVisitDate.TabIndex = 1;
            // 
            // buttonAddVisit
            // 
            this.buttonAddVisit.Location = new System.Drawing.Point(122, 263);
            this.buttonAddVisit.Name = "buttonAddVisit";
            this.buttonAddVisit.Size = new System.Drawing.Size(178, 23);
            this.buttonAddVisit.TabIndex = 0;
            this.buttonAddVisit.Text = "Добавить посещение";
            this.buttonAddVisit.UseVisualStyleBackColor = true;
            this.buttonAddVisit.Click += new System.EventHandler(this.buttonAddVisit_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.файлToolStripMenuItem,
            this.выгрузкаToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(450, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.закрытьToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.закрытьToolStripMenuItem.Text = "Закрыть";
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.закрытьToolStripMenuItem_Click);
            // 
            // выгрузкаToolStripMenuItem
            // 
            this.выгрузкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allPatientsReportToolStripMenuItem,
            this.allDiseasesReportToolStripMenuItem,
            this.visitsReportToolStripMenuItem});
            this.выгрузкаToolStripMenuItem.Name = "выгрузкаToolStripMenuItem";
            this.выгрузкаToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.выгрузкаToolStripMenuItem.Text = "Отчеты";
            // 
            // allPatientsReportToolStripMenuItem
            // 
            this.allPatientsReportToolStripMenuItem.Name = "allPatientsReportToolStripMenuItem";
            this.allPatientsReportToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.allPatientsReportToolStripMenuItem.Text = "Список всех пациентов";
            this.allPatientsReportToolStripMenuItem.Click += new System.EventHandler(this.allPatientsReportToolStripMenuItem_Click);
            // 
            // allDiseasesReportToolStripMenuItem
            // 
            this.allDiseasesReportToolStripMenuItem.Name = "allDiseasesReportToolStripMenuItem";
            this.allDiseasesReportToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.allDiseasesReportToolStripMenuItem.Text = "Список пациентов по диагнозам";
            this.allDiseasesReportToolStripMenuItem.Click += new System.EventHandler(this.allDiseasesReportToolStripMenuItem_Click);
            // 
            // visitsReportToolStripMenuItem
            // 
            this.visitsReportToolStripMenuItem.Name = "visitsReportToolStripMenuItem";
            this.visitsReportToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.visitsReportToolStripMenuItem.Text = "Список посещений";
            this.visitsReportToolStripMenuItem.Click += new System.EventHandler(this.visitsReportToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 367);
            this.Controls.Add(this.groupBoxVisit);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Программа учета посещения пациентов";
            this.groupBoxVisit.ResumeLayout(false);
            this.groupBoxVisit.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Button buttonEditPatient;
        private GroupBox groupBoxVisit;
        private Label labelVisitDate;
        private Label labelSelectPatient;
        private DateTimePicker dateTimePickerVisitDate;
        private Button buttonAddVisit;
        public  TextBox textBoxSelectPatient;
        private TextBox textBoxComment;
        private Label labelComment;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem файлToolStripMenuItem;
        private ToolStripMenuItem выгрузкаToolStripMenuItem;
        private ToolStripMenuItem allPatientsReportToolStripMenuItem;
        private ToolStripMenuItem allDiseasesReportToolStripMenuItem;
        private Button buttonAddPatient;
        private ToolStripMenuItem закрытьToolStripMenuItem;
        private ToolStripMenuItem visitsReportToolStripMenuItem;
        private ToolStripMenuItem оПрограммеToolStripMenuItem;
    }
}