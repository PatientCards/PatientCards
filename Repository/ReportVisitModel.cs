﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PatientCards.Repository
{
    public class ReportVisitModel
    {
        public int Id { get; set; }
        public DateTime VisitDate { get; set; }
        public string? Comment { get; set; }
        public int PatientId { get; set; }
    }
}
