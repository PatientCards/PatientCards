﻿using Microsoft.EntityFrameworkCore;
using PatientCards.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PatientCards.Repository;
using PatientCards.Forms;
using PatientCards.View;

namespace PatientCards
{
    public partial class ReportVisitForm : Form
    {
        private readonly RepositoryDB _repositoryDB;
        private readonly VisitManager _visitManager;
        public ReportVisitForm(RepositoryDB repositoryDB, VisitManager visitManager)
        {
            InitializeComponent();
            _repositoryDB = repositoryDB;
            _visitManager = visitManager;
        }

        private void buttonReportVisitShowVisits_Click(object sender, EventArgs e)
        {
            dataGridViewVisits.DataSource = _repositoryDB.GetVisitsForPatient(textBoxReportVisitSelectPatient.Text);
        }

        private void ReportVisitForm_Shown(object sender, EventArgs e)
        {
            dataGridViewVisits.DataSource = _repositoryDB.GetVisitsForPatient(textBoxReportVisitSelectPatient.Text);
        }

        private void textBoxReportVisitSelectPatient_Enter(object sender, EventArgs e)
        {
            string[] str = _repositoryDB.GetPatientFullNames();
            //CommonFunctions.CreateAutoCompletion(str, textBoxReportVisitSelectPatient);
            textBoxReportVisitSelectPatient.CreateAutoCompletion(str);
        }

        public void LoadPatientFromVisitManager()
        {
            if (_visitManager.Patient != null)
            {
                textBoxReportVisitSelectPatient.Text = _visitManager.Patient.GetFullName();        
            }
            else
            {
                textBoxReportVisitSelectPatient.Text = string.Empty;
            }
        }
    }
}
