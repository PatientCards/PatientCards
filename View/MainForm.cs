﻿using Microsoft.EntityFrameworkCore;
using PatientCards.Domain;
using PatientCards.Forms;
using PatientCards.Repository;
using System.Configuration;
using PatientCards.Settings;
using System.Reflection;
using PatientCards.View;

namespace PatientCards
{
    public partial class MainForm : Form
    {
        private readonly VisitManager _visitManager;
        private readonly AddEditPatientForm _patientForm;
        private readonly RepositoryDB _repositoryDB;
        private readonly ReportPatientsForm _reportPatientsForm;
        private readonly ReportDiseasesForm _reportDiseasesForm;
        private readonly ReportVisitForm _reportVisitForm;
        private readonly Settings.SettingsProvider _settingsProvider;
        public MainForm(
            VisitManager visitManager, 
            AddEditPatientForm patientForm,
            RepositoryDB repositoryDB,
            ReportPatientsForm reportPatientsForm,
            ReportDiseasesForm reportDiseasesForm,
            ReportVisitForm reportVisitForm,
            Settings.SettingsProvider settingsProvider
            )
        {
            InitializeComponent();
            _visitManager = visitManager;
            _patientForm = patientForm;
            _repositoryDB = repositoryDB;
            _reportPatientsForm = reportPatientsForm;
            _reportDiseasesForm = reportDiseasesForm;
            _reportVisitForm = reportVisitForm;
            _settingsProvider = settingsProvider;
        }

        private void buttonAddVisit_Click(object sender, EventArgs e)
        {
            Patient patient = _repositoryDB.FindPatient(textBoxSelectPatient.Text);
            if (patient != null)
            {
                _repositoryDB.AddVisit(patient.Id, textBoxComment.Text, dateTimePickerVisitDate.Value.Date);
                MessageBox.Show($"Запись о посещении добавлена. \n\nПациент: {textBoxSelectPatient.Text}" +
                    $"\nДата: {dateTimePickerVisitDate.Value.ToShortDateString()} " +
                    $"\nКомментарий: {textBoxComment.Text}", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Посещение не добавлено, т.к. не выбран пациент!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonEditPatient_Click(object sender, EventArgs e)
        {
            //reset visit date
            dateTimePickerVisitDate.Value = DateTime.Now.Date;
            
            Patient? patient = _repositoryDB.FindPatient(textBoxSelectPatient.Text);

            if (patient != null)
            {
                _visitManager.SetPatient(patient);
                _patientForm.LoadPatientFromVisitManager();
                _patientForm.Owner = this;
                _patientForm.ShowDialog();
            }
            else
                MessageBox.Show("Пациент не найден. Выберите пациента.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonAddPatient_Click(object sender, EventArgs e)
        {
            //reset visit date
            dateTimePickerVisitDate.Value = DateTime.Now.Date;


            //AddEditPatientForm patientForm = new AddEditPatientForm(_visitManager);
            _visitManager.SetPatient(null);
            _patientForm.LoadPatientFromVisitManager();
            _patientForm.Owner = this;
            _patientForm.ShowDialog();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void allPatientsReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _reportPatientsForm.ShowDialog();
        }

        private void allDiseasesReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ReportDiseasesForm reportForm = new ReportDiseasesForm();
            _reportDiseasesForm.ShowDialog();
        }

        private void textBoxSelectPatient_Click(object sender, EventArgs e)
        {
            string[] str = _repositoryDB.GetPatientFullNames();

            //CommonFunctions.CreateAutoCompletion(str, textBoxSelectPatient);
            textBoxSelectPatient.CreateAutoCompletion(str);
        }

        private void visitsReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Patient? patient = _repositoryDB.FindPatient(textBoxSelectPatient.Text);

            if (patient != null)
            {
                //AddEditPatientForm patientForm = new AddEditPatientForm(_visitManager);
                _visitManager.SetPatient(patient);
                _reportVisitForm.LoadPatientFromVisitManager();
            }
            else
            {
                _visitManager.SetPatient(null);
            }
            //_reportVisitForm.Owner = this;
            _reportVisitForm.ShowDialog();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Программа предназначена для ведения карточек и учёта визита пациентов" +
                $"\n" +
                //$"\nВерсия программы: {VersionNumber.VERSION_NUMBER}" +
                $"\nВерсия программы: {PatientCards.Settings.VersionNumber.GetInformationalVersion(Assembly.GetExecutingAssembly())}" +
                $"\n" +
                $"\nКак пользоваться:" +
                $"\n1) Внесите пациентов в базу, используя кнопку \"Добавить нового пациента\"" +
                $"\n2) Выберите пациента в поле \"Выберите пациента\", начиная вводить его фамилию. Программа сама предложит пациента, если он есть в базе" +
                $"\n3) Выберите дату посещения" +
                $"\n4) Добавьте комментарий к данному посещению в произвольной форме (необязательно)" +
                $"\n5) Нажмите кнопку \"Добавить посещение\"" +
                $"\n" +
                $"\nВ программе реализованы следующие отчёты:" +
                $"\n1) Список всех пациентов" +
                $"\n2) Список пациентов с выбранным диагнозом" +
                $"\n3) Список посещений выбранного пациента", 
                "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /*private async void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text.Length >= 3)
            {
                comboBox1.Items.Add("item1");
                comboBox1.Items.Add("item2");
                comboBox1.Items.Add("item3");
                comboBox1.Items.Add("item4");
                string temp = comboBox1.Text;
                comboBox1.Items.Clear();
                comboBox1.Text = temp;

                //remove all before selected item
                while(comboBox1.SelectedIndex > 0)
                    comboBox1.Items.RemoveAt(0);
                //remove all after selected item
                while (comboBox1.Items.Count > 1)
                    comboBox1.Items.RemoveAt(1);

                //comboBox1.Items.Clear();
                //comboBox1.Items.Add(temp);
                for (int i = 0; i < comboBox1.Items.Count; i++)
                {
                    if (comboBox1.SelectedIndex != i)
                        comboBox1.Items.RemoveAt(i);
                }
                //    comboBox1.Items.RemoveAt(0);
                //comboBox1.Text = temp;

                comboBox1.Items.AddRange(await CommonFunctions.GetAddressFromDaData(comboBox1.Text));
                //comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
                //comboBox1.AutoCompleteMode = AutoCompleteMode.Suggest;
                //comboBox1.DroppedDown = true;

                //comboBox1.Text = temp;

                //Thread.Sleep(1000);

                //CommonFunctions.CreateAutoCompletion(await CommonFunctions.GetAddressFromDaData(textBoxAddress.Text), textBoxAddress);
            }
        }

        private async void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 3)
            {
                AutoCompleteStringCollection source = new AutoCompleteStringCollection();
                string[] str = await CommonFunctions.GetAddressFromDaData(textBox1.Text);
                source.AddRange(str);
                textBox1.AutoCompleteCustomSource = source;
                textBox1.AutoCompleteMode = AutoCompleteMode.Suggest;
                textBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;


                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(str);
                comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
                comboBox1.AutoCompleteMode = AutoCompleteMode.Suggest;
                comboBox1.DroppedDown = true;

                //Thread.Sleep(1000);
                //CommonFunctions.CreateAutoCompletion(str, textBox1);

                //CommonFunctions.CreateAutoCompletion(await CommonFunctions.GetAddressFromDaData(textBoxAddress.Text), textBoxAddress);
            }
        }

        private async void textBox1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length >= 3)
            {
                AutoCompleteStringCollection source = new AutoCompleteStringCollection();
                string[] str = await CommonFunctions.GetAddressFromDaData(textBox1.Text);
                source.AddRange(str);
                textBox1.AutoCompleteCustomSource = source;
                textBox1.AutoCompleteMode = AutoCompleteMode.Suggest;
                textBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;


                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(str);
                comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
                comboBox1.AutoCompleteMode = AutoCompleteMode.Suggest;
                comboBox1.DroppedDown = true;

                //Thread.Sleep(1000);
                //CommonFunctions.CreateAutoCompletion(str, textBox1);

                //CommonFunctions.CreateAutoCompletion(await CommonFunctions.GetAddressFromDaData(textBoxAddress.Text), textBoxAddress);
            }

        }*/
    }
}