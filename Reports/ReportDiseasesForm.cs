﻿using Microsoft.EntityFrameworkCore;
using PatientCards.Domain;
using PatientCards.Repository;
using PatientCards.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PatientCards
{
    public partial class ReportDiseasesForm : Form
    {
        private readonly RepositoryDB _repositoryDB;
        public ReportDiseasesForm(RepositoryDB repositoryDB)
        {
            InitializeComponent();
            _repositoryDB = repositoryDB;
        }

        private void textBoxSelectDiseaseReport_Click(object sender, EventArgs e)
        {
            //_repositoryDB.GetStringsForAutoCompletion(textBoxSelectDiseaseReport, AutoCompletionType.Disease);
            //CommonFunctions.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.Disease), textBoxSelectDiseaseReport);
            textBoxSelectDiseaseReport.CreateAutoCompletion(_repositoryDB.GetStringsForAutoCompletion(AutoCompletionType.Disease));
        }

        private void buttonPatientsWithDiseaseShow_Click(object sender, EventArgs e)
        {
            dataGridViewPatientsWithDisease.DataSource = _repositoryDB.GetPatientsWithDisease(textBoxSelectDiseaseReport.Text, checkBoxRegOnly.Checked);
            
                /*using (ApplicationContext db = new ApplicationContext())
            {
                Disease? disease = db.Diseases.Where(d => d.Name == textBoxSelectDiseaseReport.Text).FirstOrDefault();
                if (disease != null)
                {
                    if(checkBoxRegOnly.Checked)
                    {
                        db.Patients.Where(p => p.Disease == disease).Where(p => p.IsUnregistred == false).Load();
                    }
                    else 
                        db.Patients.Where(p => p.Disease == disease).Load();
                    //db.Diseases.Load();
                    //dataGridViewPatientsWithDisease.DataSource = db.Patients.Where(p => p.Disease == disease).tob tob;
                    dataGridViewPatientsWithDisease.DataSource = db.Patients.Local.ToBindingList();
                    //dataGridViewPatientsWithDisease.DataSource = db.Patients.Local.ToBindingList();
                }
            } */
        }
    }
}
